import struct
import os
import argparse


class TdmsChecker:
    def __init__(self):
        self.segments = []

    def check_tag(self, f):
        data = f.read(4)
        if data != 'TDSm'.encode(encoding='ascii'):
            raise ValueError('Ungültiges TAG: ' + str(data))
        self.segments.append(f.tell() - 4)

    def check_toc(self, f):
        data = f.read(4)
        # noch keine Implementierung


    def check_version(self, f):
        data = f.read(4)
        version = struct.unpack('i', data)[0]
        if version != 4713:
            raise ValueError('Ungültige Version: ' + str(version))


    def check_nextseg(self, f, fix):
        data = f.read(8)
        offset = struct.unpack('q', data)[0]
        remaining_size = os.fstat(f.fileno()).st_size - f.tell()
        if offset > remaining_size:
            print("Warnung: Offset={0}, Restgrösse={1}".format(offset, remaining_size))
            if fix:
                print("Korrektur " + str(self.segments[-1]))
                f.truncate(self.segments[-1])
                return
        f.seek(offset + 8, 1)

    def is_eof(self, f):
        return f.tell() >= os.fstat(f.fileno()).st_size


    def check(self, tdms_file, fix=False):
        with open(tdms_file, 'r+b' if fix else 'rb') as f:
            segment = 1
            while not self.is_eof(f):
                if self.check_tag(f):
                    break
#                print("Prüfe Segment {0}".format(segment))
                self.check_toc(f)
                self.check_version(f)
                self.check_nextseg(f, fix)
                segment += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('tdms_file', help="TDMS-File")
    parser.add_argument('--fix', help="Korrigiert Offset-Fehler", action='store_true')
    options = parser.parse_args()

    check = TdmsChecker()
    check.check(options.tdms_file, options.fix)

