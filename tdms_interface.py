# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import nptdms
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
from check_tdms import TdmsChecker
###############################################################################
# Header
###############################################################################
__author__ = ['Benjamin Loeffel']
__maintainer__ = ['Benjamin Loeffel']
__email__ = ['benjamin.loeffel@fischerspindle.ch']
###############################################################################
# Classes
###############################################################################
class TDMS:
    def __init__(self,tdms_file_name,check_tdms=False):
        """
        Paramters
        ---------
        tdms_file_name : scalar,string
            path to the tdms file
        check_tdms : scalar, bool, optional
            flag for automatic correction of corrupted files, default = False
        """     
        if check_tdms==True:
            print("checking tdms file")
            print(os.path.abspath(tdms_file_name))
            checker=TdmsChecker()
            checker.check(tdms_file_name,fix=True)
        self.tdms_file=nptdms.TdmsFile(tdms_file_name)
###############################################################################
    def get_channel_data(self,channel_name,group=None):
        """
        Gets the data of the specified channel
        
        Parameters
        ----------
        channel_name : scalar,string
            name of the channel to get data from
        group : scalar,string,optional
            name of the group containing the channel, defaut = None so the first group gets taken
            
        Returns
        -------
        data : np.array
            data read from channel
        """
        if group==None:
            group=self.tdms_file.groups()[0]
            
        tdms_object=self.tdms_file.object(group,channel_name)
        data=tdms_object.data
        return data
    
    def get_channel_time(self,channel_name,group=None,mode="relative"):
        """
        Returns time track of the specified channel
        
        Parameters
        ----------
        channel_name : scalar,string
            name of the channel to get data from
        group : scalar,string,optional
            name of the group containing the channel, defaut = None so the first group gets taken
        mode : scalar, string, optional
            time stamp mode, either 'relative' or 'absolut', default = 'relative'
        
        Returns
        -------
        time_stamps : np.array
            array containing the timestamps, either datetime.datetime or seconds
        """
        if group==None:
            group=self.tdms_file.groups()[0]          
        tdms_object=self.tdms_file.object(group,channel_name)
        if mode=="absolut":
            return tdms_object.time_track(absolute_time=True)
        elif mode=="relative":
            return tdms_object.time_track(absolute_time=False)
        else:
            raise ValueError("mode can be 'absolut' or 'relative', nothing else")
            
    def get_channel_unit(self,channel_name,group=None):
        """
        Gets the unit of the specified channel
        
        Parameters
        ----------
        channel_name : scalar,string
            name of the channel to get data from
        group : scalar,string,optional
            name of the group containing the channel, defaut = None so the first group gets taken
            
        Returns
        -------
        unit : scalar, string
            unit of the channel
        """        
        if group==None:
            group=self.tdms_file.groups()[0]
            
        tdms_object=self.tdms_file.object(group,channel_name)
        
        unit=tdms_object.property('NI_UnitDescription')

        return unit
    
    def get_time_channel(self,mode="relative"):
        """
        Returns time channel, special method for FENG
        
        Parameters
        ----------
        mode : scalar, string, optional
            time stamp mode, either 'relative' or 'absolut', default = 'relative'
        
        Returns
        -------
        time_stamps : np.array
            array containing the timestamps, either datetime.datetime or seconds
        """
        time_stamps=self.get_channel_data(channel_name="Time")
        if mode=="absolut":
            return time_stamps
        elif mode=="relative":
            time_stamps_relative=np.zeros(len(time_stamps))
            for index,time_stamp in enumerate(time_stamps):
                time_stamps_relative[index]=(time_stamp-time_stamps[0]).total_seconds()
            return time_stamps_relative
        else:
            raise ValueError("mode can be 'absolut' or 'relative', nothing else")
###############################################################################
    def get_group_names(self):
        """
        Returns all group names contained in tdms file
        
        Returns
        -------
        groups : list, string
            list of group names
        """
        return self.tdms_file.groups()    
    
    def get_group_channel_names(self,group=None):
        """
        Returns all channel names in specified group
        
        Parameters
        ----------
        group : scalar,string,optional
            name of the group, defaut = None so the first group gets taken
        
        Returns
        -------
        channel_names : list
            list of channel names contained in group
        """
        if group==None:
            group=self.tdms_file.groups()[0]
        channels=self.tdms_file.group_channels(group)
        channel_names=[]
        for channel in channels:
            channel_names.append(channel.channel)
        return channel_names
###############################################################################
    def get_property(self,key):
        """
        Returns the value of the specified key
        
        Parameters
        ----------
        key : scalar, string
            key to desired value
        
        Returns
        -------
        value
            retreived value from key
        """
        return self.tdms_file.object().property(key)

    def get_properties(self):
        """
        Returns all root properties
        
        Returns
        -------
        properties : collections.OrderedDict
            ordered dict containing key/values of all root properties
        """
        return self.tdms_file.object().properties  
###############################################################################
# Methods
###############################################################################
def read_sliced_tdms(root_dir,tdms_file_name_template,channel_name,group=None):
    sub_directories=glob.glob(root_dir+"/*/")
    data=[]
    for sub_directory in sub_directories:
        print(sub_directory)
        tdms_file_name=os.path.join(root_dir,sub_directory,tdms_file_name_template)
        tdms=TDMS(tdms_file_name=tdms_file_name)
        data_slice=tdms.get_channel_data(channel_name=channel_name,group=group)
        data.append(data_slice)    
    data=np.hstack(data)
    return data
###############################################################################
###############################################################################
if __name__ == "__main__":
    plt.close("all")
    
    if "read" not in locals():
        read=True
        
        tdms_file_name="FR00050_004.tdms"
                
        tdms=TDMS(tdms_file_name=tdms_file_name)
        tdms_file=tdms.tdms_file
                     
    properties=tdms.get_properties()
    keys=properties.keys()  
    print("")
    print("Umrichter = {umrichter}".format(umrichter=tdms.get_property("Umrichter")))
    
    groups=tdms.get_group_names()
    group=groups[0]
    channel_names=tdms.get_group_channel_names(group=group)
    print("")
    print("Channel names")
    print(channel_names)
    
    channel="AI004_Druck"
    time_absolut=tdms.get_time_channel(mode="absolut")
    time_relative=tdms.get_time_channel(mode="relative")
    data=tdms.get_channel_data(channel_name=channel)
    unit=tdms.get_channel_unit(channel_name=channel)

    plt.figure()
    plt.subplot(2,1,1)
    plt.plot(time_absolut,data,label="data")
    plt.title("Data over absolut time")
    plt.xlabel("time")
    plt.ylabel("data / {unit}".format(unit=unit))
    plt.legend()
    plt.grid("on")
        
    plt.subplot(2,1,2)
    plt.plot(time_relative,data,label="data")
    plt.title("Data over relative time")
    plt.xlabel("time")
    plt.ylabel("data / {unit}".format(unit=unit))
    plt.legend()
    plt.grid("on")